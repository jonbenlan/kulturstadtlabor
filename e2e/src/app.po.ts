import {browser, by, element, protractor} from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get(browser.baseUrl) as Promise<any>;
  }

  getTitleTextRessortOverview() {
    return element(by.id('ressortOverview_title')).getText() as Promise<string>;
  }

   clickRessortKasse() {
     return element(by.id('ressortOverview_ressortLink_Kasse')).click();
  }

  clickButtonBack() {
    return element(by.id('button_back')).click();
  }

  getTitleTextRessortDetail() {
    return element(by.id('ressortDetail_title')).getText() as Promise<string>;
  }
}
