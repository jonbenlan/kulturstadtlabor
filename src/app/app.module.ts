import {BrowserModule} from '@angular/platform-browser';
import {LOCALE_ID, NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {environment} from 'src/environments/environment';
import {AngularFireModule} from '@angular/fire';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import {AngularFireStorageModule} from '@angular/fire/storage';
import {ReactiveFormsModule} from '@angular/forms';
import {PageNotFoundModule} from './page-not-found/page-not-found.module';
import {CoreModule} from './core/core.module';
import {AuthService} from './authentication/service/auth.service';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {HeaderComponent} from './header/components/header/header.component';
import {AppNavComponent} from './header/components/app-nav/app-nav.component';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {HttpClientModule} from '@angular/common/http';
import {InlineSVGModule} from 'ng-inline-svg';
import {RouterModule} from '@angular/router';
import {StoreModule} from '@ngrx/store';
import {reducers} from './reducers/app.reducer';
import {RessortModule} from './ressort/ressort.module';
import {EffectsModule} from '@ngrx/effects';
import {ConfirmationDialogComponent} from './shared/confirmation-dialog/confirmation-dialog.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {FlexLayoutModule, FlexModule} from '@angular/flex-layout';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule} from '@angular/material';
import { registerLocaleData } from '@angular/common';
import localeDe from '@angular/common/locales/de';
registerLocaleData(localeDe, 'de-DE');

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    AppNavComponent,
    ConfirmationDialogComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireAuthModule,
    ReactiveFormsModule,
    RessortModule,
    CoreModule,
    BrowserAnimationsModule,
    HttpClientModule,
    InlineSVGModule.forRoot(),
    StoreModule.forRoot(reducers),
    StoreDevtoolsModule.instrument({
      maxAge: 10
    }),
    EffectsModule.forRoot([]),
    RouterModule,
    MatDialogModule,
    MatButtonModule,
    MatSnackBarModule,
    FlexModule,
    FlexLayoutModule,
    PageNotFoundModule,
  ],
  entryComponents: [
    ConfirmationDialogComponent
  ],
  providers: [AuthService, {provide: LOCALE_ID, useValue: 'de-DE' }],
  exports: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
