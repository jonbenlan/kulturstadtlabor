import {createAction, props} from '@ngrx/store';
import {User as fireBaseUser} from 'firebase/app';
import {User} from '../../models/user';


const USER_CHANGED = '[Core] User changed';
const ADD = '[Core] Add message';
const REMOVE = '[Core] Remove message';
const LOGIN = '[Core] Login';
const SIGN_UP = '[Core] Sign-up';
const SIGN_OUT = '[Core] SignOut';
const CREATE_METADATA = '[Core] Create metadata for user';
const UPDATE_METADATA = '[Core] Update metadata for user';
const METADATA_CHANGED = '[Core] Metadata changed';
const RESET_PASSWORD = '[Core] Reset Password';

export const UserChanged = createAction(USER_CHANGED, props<{ user: fireBaseUser }>());

export const AddMessage = createAction(ADD, props<{ message: string }>());

export const RemoveMessage = createAction(REMOVE, props<{ item: any }>());

export const LoginUser = createAction(LOGIN, props<{ email: string, password: string }>());

export const SignUpUser = createAction(SIGN_UP, props<{ userData: User }>());

export const SignOutUser = createAction(SIGN_OUT, props<{}>());

export const UpdateMetadataForUser = createAction(UPDATE_METADATA, props<{ user: User }>());

export const CreateMetadataForUser = createAction(CREATE_METADATA, props<{ user: User }>());

export const MetadataChanged = createAction(METADATA_CHANGED, props<{ user: User }>());

export const ResetPassword = createAction(RESET_PASSWORD, props<{email: string}>());
