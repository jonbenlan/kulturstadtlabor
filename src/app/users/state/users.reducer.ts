import {Action, createReducer, on} from '@ngrx/store';
import * as usersActions from './users.action';
import {initialState, usersAdapter, UsersState} from './users.state';

export function usersReducer(state: UsersState | undefined, action: Action) {
  return reducer(state, action);
}

const reducer = createReducer(
  initialState,
  on(usersActions.UsersChanged, (state, {users}) => {
    state = usersAdapter.removeAll(state);
    return usersAdapter.addMany(users, state);
  }),
);
