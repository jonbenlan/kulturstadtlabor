import {Injectable} from '@angular/core';
import {AuthService} from '../../authentication/service/auth.service';
import {Actions} from '@ngrx/effects';
import {AngularFireStorage} from '@angular/fire/storage';
import {Store} from '@ngrx/store';
import * as fromUsers from './users.state';


@Injectable()
export class UsersEffects {

  constructor(
    private actions$: Actions,
    private authService: AuthService,
    public store: Store<fromUsers.UsersState>,
    public afstorage: AngularFireStorage,
  ) {
  }

lol
}
