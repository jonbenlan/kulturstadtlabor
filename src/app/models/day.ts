export interface Day {
  id: string;
  date: Date;
}
