import {AdditionalFields} from './additionalFields';

export interface Ressort {
  id: string;
  title: string;
  description: string;
  email?: string;
  visibility?: boolean;
  additionalFields?: AdditionalFields[];
}
