import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {StoreModule} from '@ngrx/store';
import {CreateRessortComponent} from './components/create-ressort/create-ressort.component';
import {ressortReducer} from './state/ressort.reducer';
import {RessortsRoutingModule} from './ressorts-routing.module';
import {RessortsDetailviewComponent} from './components/ressorts-detailview/ressorts-detailview.component';
import {RessortsOverviewComponent, RessortsOverviewWrapperComponent} from './components/ressorts-overview/ressorts-overview.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatButtonModule, MatCheckboxModule, MatGridListModule, MatIconModule, MatInputModule} from '@angular/material';
import {FlexLayoutModule} from '@angular/flex-layout';
import {EffectsModule} from '@ngrx/effects';
import {RessortEffects} from './state/ressort.effects';
import {EditRessortComponent, EditRessortWrapperComponent} from './components/edit-ressort/edit-ressort.component';
import {ShiftModule} from '../shift/shift.module';
import {CalendarModule} from '../calendar/calendar.module';
import {BackButtonModule} from '../shared/back-button/back-button.module';
import {UsersExportWrapperComponent} from '../shared/users-export/users-export.component';

@NgModule({
  declarations: [
    CreateRessortComponent,
    EditRessortComponent,
    EditRessortWrapperComponent,
    RessortsDetailviewComponent,
    RessortsOverviewComponent,
    RessortsOverviewWrapperComponent
  ],
  imports: [
    CommonModule,
    StoreModule.forFeature('ressort', ressortReducer),
    EffectsModule.forFeature([RessortEffects]),
    RessortsRoutingModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatGridListModule,
    MatButtonModule,
    MatIconModule,
    FlexLayoutModule,
    ShiftModule,
    CalendarModule,
    BackButtonModule,
    MatCheckboxModule
  ],
  exports: [
    CreateRessortComponent,
    RessortsDetailviewComponent,
    RessortsOverviewComponent,
    UsersExportWrapperComponent,
    RessortsOverviewWrapperComponent
  ]
})
export class RessortModule {
}
