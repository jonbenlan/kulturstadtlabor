import {Action, createReducer, on} from '@ngrx/store';
import * as ressortActions from './ressort.action';
import {initialState, ressortAdapter, RessortState} from './ressort.state';

export function ressortReducer(state: RessortState | undefined, action: Action) {
  return reducer(state, action);
}

const reducer = createReducer(
  initialState,
  on(ressortActions.RessortsChanged, (state, {ressorts}) => {
    state = ressortAdapter.removeAll(state);
    return ressortAdapter.addMany(ressorts, state);
  })
);
