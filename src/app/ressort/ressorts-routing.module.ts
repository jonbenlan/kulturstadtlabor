import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RessortsOverviewWrapperComponent} from './components/ressorts-overview/ressorts-overview.component';
import {RessortsDetailviewComponent} from './components/ressorts-detailview/ressorts-detailview.component';
import {ressortRoutesNames} from './ressorts.route.names';
import {EditRessortWrapperComponent} from './components/edit-ressort/edit-ressort.component';


const routes: Routes = [
  {
    path: ressortRoutesNames.BASE, component: RessortsOverviewWrapperComponent
  },
  {
    path: ressortRoutesNames.DETAIL, component: RessortsDetailviewComponent
  },
  {
    path: ressortRoutesNames.EDIT, component: EditRessortWrapperComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RessortsRoutingModule {
}
