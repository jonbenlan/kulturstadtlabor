import {Injectable} from '@angular/core';

import {from, Subscription} from 'rxjs';
import {map} from 'rxjs/operators';
import {AngularFirestore} from '@angular/fire/firestore';
import * as RessortActions from '../state/ressort.action';
import * as fromRessort from '../state/ressort.state';
import {Ressort} from '../../models/ressorts';
import {Store} from '@ngrx/store';
import {ShiftService} from '../../shift/service/shift.service';
import {AdditionalFields} from '../../models/additionalFields';

@Injectable({providedIn: 'root'})
export class RessortService {

  private subscription: Subscription[] = [];
  private collectionName = 'ressort';


  constructor(private store: Store<fromRessort.RessortState>, protected db: AngularFirestore, private shiftService: ShiftService) {
    this.clearSubscription();

    this.addSubscription(db.collection<Ressort>(this.collectionName)
      .snapshotChanges()
      .pipe(map(actions => {
        return actions.map(action => {
          return {id: action.payload.doc.id, ...action.payload.doc.data()} as Ressort;
        });
      })).subscribe((items) => this.ressortsChanged(items)));
  }

  clearSubscription() {
    this.subscription.forEach(x => x.unsubscribe());
    this.subscription = [];
  }

  addSubscription(subscription: Subscription) {
    this.subscription.push(subscription);
  }

  ressortsChanged(ressorts: Ressort[]) {
    this.store.dispatch(RessortActions.RessortsChanged({ressorts}));
  }


  getRessort(id: string) {
    return this.db.doc(`${this.collectionName}/${id}`);
  }

  addRessort(title: string, description: string, email: string, visibility: boolean, additionalFields: AdditionalFields[]) {
    const id = this.db.createId();
    const ressort: Ressort = {
      id,
      title,
      description,
      email,
      visibility,
      additionalFields
    };
    return from(this.getDoc(id).set(ressort));
  }

  updateRessort(ressort: Ressort) {
    return from(this.getDoc(ressort.id).update(ressort));
  }

  removeRessort(id: string) {
    return from(this.getDoc(id).delete());
  }

  get collection() {
    return this.db.collection<Ressort>(this.collectionName);
  }

  getDoc(id: string) {
    return this.db.doc(`${this.collectionName}/${id}`);
  }
}
