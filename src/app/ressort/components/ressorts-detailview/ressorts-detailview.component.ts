import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder} from '@angular/forms';
import {Store} from '@ngrx/store';
import * as fromRessort from '../../state/ressort.state';
import {Observable} from 'rxjs';
import {Ressort} from '../../../models/ressorts';
import {ConfirmationDialogComponent} from '../../../shared/confirmation-dialog/confirmation-dialog.component';
import {MatDialog} from '@angular/material/dialog';
import {ressortRoutesNames} from '../../ressorts.route.names';
import {ConfirmDialogInformation} from '../../../models/confirmDialogInformation';
import * as RessortActions from '../../state/ressort.action';
import * as ShiftActions from '../../../shift/state/shift.action';
import * as fromShift from '../../../shift/state/shift.state';
import {Shift} from '../../../models/shift';
import * as fromCore from '../../../core/state/core.state';

@Component({
  selector: 'app-ressorts-detailview',
  templateUrl: './ressorts-detailview.component.html',
  styleUrls: ['./ressorts-detailview.component.scss']
})
export class RessortsDetailviewComponent implements OnInit {

  public workerIds: string[] = [];


  @Input('isAdmin')
  public isAdmin: boolean;
  public isLoggedIn$: Observable<boolean>;
  public isAdmin$: Observable<boolean>;

  public ressort$: Observable<Ressort>;

  public shifts$: Observable<Shift[]>;


  constructor(
    public router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private ressortStore: Store<fromRessort.RessortState>,
    public dialog: MatDialog,
    private coreStore: Store<fromCore.State>,
  ) {
  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.ressort$ = this.ressortStore.select(
      fromRessort.selectRessort(id));
    this.shifts$ = this.ressortStore.select(fromShift.selectShiftsForRessort(id));
    this.isLoggedIn$ = this.coreStore.select(fromCore.isLoggedIn);
    this.isAdmin$ = this.coreStore.select(fromCore.isAdmin);

  }

  public getWorkerIds(shifts) {
    shifts.map(shift => {
      shift.workers.map(worker => {
        if (!this.workerIds.includes(worker)) {
          this.workerIds.push(worker);
        }
      });
    });
    return this.workerIds;
  }

  public getWorkers(shifts) {

    if (shifts.filter(shift => {
      return shift.workers.length > 0;
    }).length) {
      return true;
    }

  }

  removeRessort(ressort: Ressort) {
    this.ressortStore.dispatch(ShiftActions.RemoveShiftsForRessort({ressortId: ressort.id}));
    this.ressortStore.dispatch(RessortActions.RemoveRessort({id: ressort.id}));
  }

  editRessort(ressort: Ressort) {
    const path = ressortRoutesNames.EDIT.replace(':id', ressort.id);
    this.router.navigate([path]);
  }

  openDialog(ressort: Ressort): void {
    const popupTitle = `Ressort ${ressort.title}`;
    const popupMessage = `Soll das Ressort  wirklich gelöscht werden?`;
    const dialogInformation: ConfirmDialogInformation = {
      title: popupTitle,
      message: popupMessage
    };
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: dialogInformation,
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.removeRessort(ressort);
        this.router.navigate([ressortRoutesNames.BASE]);
      }
    });
  }
}
