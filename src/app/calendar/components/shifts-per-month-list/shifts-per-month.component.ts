import {Component, Input, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromCalendar from '../../state/calendar.state';
import * as CalendarActions from '../../state/calendar.action';
import {Ressort} from '../../../models/ressorts';
import {Shift} from '../../../models/shift';
import {first} from "rxjs/operators";

@Component({
  selector: 'app-shifts-per-month',
  templateUrl: './shifts-per-month.component.html',
  styleUrls: ['./shifts-per-month.component.scss']
})

export class ShiftsPerMonthComponent implements OnInit {

  public ressort: Ressort;

  @Input('shifts')
  public shifts: Shift[];

  @Input('currentShiftDate')
  public currentShiftDate: Date;


  public currentMonthName: string;
  public firstShiftMonthLength: number;
  public currentMonthArray: number[];
  public currentMonthDaysWithShifts: number[];
  public weekDayLabels = ['MO', 'DI', 'MI', 'DO', 'FR', 'SA', 'SO'];
  public emptyCalendarEntries: number[];


  constructor(
    private store: Store<fromCalendar.CalendarState>,
  ) {
  }

  ngOnInit() {

    if (this.currentShiftDate) {
      this.emptyCalendarEntries = this.getEmptyCalendarEntries(this.currentShiftDate.getMonth(), this.currentShiftDate.getFullYear());
      this.firstShiftMonthLength = this.daysInMonth(this.currentShiftDate.getMonth(), this.currentShiftDate.getFullYear());
      this.currentMonthArray = Array(this.firstShiftMonthLength).fill(0).map((x, i) => i + 1);
      this.currentMonthDaysWithShifts = this.getCurrentMonthDaysWithShifts(this.shifts, this.currentShiftDate.getMonth());
      this.currentMonthName = this.currentShiftDate.toLocaleString('default', {month: 'long'});
    }

  }

  displayNextMonth() {
    this.store.dispatch(CalendarActions.NextMonth());
  }

  displayPreviousMonth() {
    this.store.dispatch(CalendarActions.PrevMonth());
  }

  public displayClickedDay(clickedDay) {

    const dates: Date[] = [];
    const clickedDayDate = new Date(this.currentShiftDate.getFullYear(), this.currentShiftDate.getMonth(), clickedDay);
    dates.push(clickedDayDate);
    this.store.dispatch(CalendarActions.UpdateDays({dates}));
    this.currentShiftDate = clickedDayDate;

  }

  private getEmptyCalendarEntries(month, year) {
    const firstWeekdayInMonth = new Date(year, month, 1).getDay();
    return Array((firstWeekdayInMonth + 6) % 7).fill(0).map((x, i) => i);
  }

  private daysInMonth(month, year) {
    return new Date(year, month + 1, 0).getDate();
  }

  private getCurrentMonthDaysWithShifts(shifts, currentMonth) {
    const daysWithShifts = [];

    shifts.forEach((shift) => {
      const shiftDate = new Date(shift.start.seconds * 1000);

      if (shiftDate.getMonth() === currentMonth) {
        daysWithShifts.push(shiftDate.getDate());
      }

    });

    return daysWithShifts;
  }
}


