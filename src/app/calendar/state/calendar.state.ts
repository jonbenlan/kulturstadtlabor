import {createEntityAdapter, EntityState} from '@ngrx/entity';
import {createFeatureSelector, createSelector} from '@ngrx/store';
import {Day} from '../../models/day';

export const calendarAdapter = createEntityAdapter<Day>();

export interface CalendarState extends EntityState<Day> {
}

export const initialState: CalendarState = calendarAdapter.getInitialState();

export const getCalendarState = createFeatureSelector<CalendarState>('calendar');

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = calendarAdapter.getSelectors(getCalendarState);

export const selectLast = () => {
  return createSelector(selectAll, x => x.reverse()[0]);
};

export const selectFirst = () => {
  return createSelector(selectAll, x => x[0]);
};


