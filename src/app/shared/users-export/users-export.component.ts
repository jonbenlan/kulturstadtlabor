import {Component, Input, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import * as fromUsers from '../../users/state/users.state';
import {User} from '../../models/user';
import {saveAs} from 'file-saver';
import {UsersService} from '../../users/service/users.service';
import {Shift} from '../../models/shift';
import {additionalFieldInitData} from '../additionalFields';

@Component({
  selector: 'app-users-export',
  templateUrl: './users-export.component.html',
})
export class UsersExportComponent implements OnInit {

  private additionalFieldsInitData = additionalFieldInitData;

  @Input('export')
  public export: {
    shift: Shift,
    workers: User[]
  }[];

  @Input('usersOnly')
  public usersOnly: boolean;

  constructor() {}

  ngOnInit() {
  }

  public async initialiseExport(exportArray) {

    const csvArray = [];
    let csvData = '';
    let exportName: string;
    const BOM = '\uFEFF';
    const replacer = (key, value) => value === null ? '' : value;

    const headerObject = {
      email: 'E-Mail',
      lastName: 'Name',
      firstName: 'Vorname',
      address: 'Strasse',
      place: 'PLZ',
      birthDate: 'Geburtstag',
      phone: 'Telefon Privat',
    };

    this.additionalFieldsInitData.forEach( field => {
      headerObject[field.id] = field.name;
    });

    /**
     * map shifts, push every shifts time, title (if any), and  workers into csvArray
     * add two breaks after each shift
     */
    exportArray.map(shiftExport => {

      if (this.usersOnly) {

        shiftExport.workers.map(row => {

          csvArray.push(Object.keys(headerObject).map(fieldName => JSON.stringify(row[fieldName], replacer)).join(','));

        });

        exportName = `ressort`;

      } else {

        const shiftStartingTime = new Date(shiftExport.shift.start.seconds * 1000);
        const shiftEndTime = new Date(shiftExport.shift.end.seconds * 1000);
        const shiftStartingMinutes = `0${shiftStartingTime.getMinutes()}`;
        const shiftEndingMinutes = `0${shiftEndTime.getMinutes()}`;
        const shiftTimeString = `${shiftStartingTime.getHours()}:${shiftStartingMinutes.substr(-2)} - ${shiftEndTime.getHours()}:${shiftEndingMinutes.substr(-2)}`;
        exportName = `${shiftStartingTime.getDate()}_${shiftStartingTime.getMonth() + 1}_tages`;

        shiftExport.workers.map(row => {
          const rowArray = Object.keys(headerObject).map(fieldName => JSON.stringify(row[fieldName], replacer));
          rowArray.unshift(shiftExport.shift.title, shiftTimeString);
          csvArray.push(rowArray.join(','));
          csvArray.push('\r\n');
        });

        csvArray.push('\r\n\r\n');

      }

    });

    if (this.usersOnly) {

      const uniq = (myArray) => {
        const seen = {};
        return myArray.filter((item) => {
          return seen.hasOwnProperty(item) ? false : (seen[item] = true);
        });
      };

      const cleanedArray = uniq(csvArray);

      csvArray.length = 0;
      csvArray.push(...cleanedArray);

      let i = csvArray.length;

      do {
        csvArray.splice(i, 0, '\r\n');
      } while (i-- > 1);

    }

    csvArray.unshift('\r\n');
    if (this.usersOnly) {
      csvArray.unshift(Object.values(headerObject).join(','));
    } else {
      const header = Object.values(headerObject);
      header.unshift('Schichtname', 'Schichtzeit');
      csvArray.unshift(header.join(','));
    }
    csvData = BOM + csvArray.join('');

    const blob = new Blob([csvData], {type: 'text/csv;charset=UTF-8' });
    saveAs(blob, `${exportName}-export.csv`, {autoBom: true});

  }

}

@Component({
  selector: 'app-users-export-wrapper',
  template: `
    <app-users-export *ngIf="export$" [export]="export$ | async" [usersOnly]="usersOnly"></app-users-export>
  `,
})

export class UsersExportWrapperComponent implements OnInit {
  public export$: Observable<{
    shift: Shift,
    workers: User[]
  }[]>;

  @Input('shifts')
  public shifts: Shift[];

  @Input('usersOnly')
  public usersOnly: boolean;

  constructor(
    private store: Store<fromUsers.UsersState>,
    private usersService: UsersService
  ) {
  }

  ngOnInit() {

    // todo: why is very often the first entry missing?? why?= or chekc whether its just joels problem.

    this.export$ = this.store.select(fromUsers.selectUsersByShift(this.shifts));

  }
}
