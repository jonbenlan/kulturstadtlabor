import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FlexLayoutModule, FlexModule} from '@angular/flex-layout';
import {MatTooltipModule} from '@angular/material/tooltip';
import {BackButtonComponent} from './component/back-button.component';
import {MatIconModule} from '@angular/material';

@NgModule({
  declarations: [
    BackButtonComponent
  ],
  exports: [
    BackButtonComponent
  ],
  imports: [
    FlexLayoutModule,
    CommonModule,
    FlexModule,
    MatTooltipModule,
    MatIconModule
  ],
  providers: [],
})
export class BackButtonModule {
}
