import {Component, Input, OnInit} from '@angular/core';
import {Shift} from '../../../models/shift';

@Component({
    selector: 'app-export-shift-per-day',
    templateUrl: './export-shift-per-day.component.html'
})
export class ExportShiftPerDayComponent implements OnInit {



    public workerIds: string[] = [];

    @Input('shifts')
    shifts: Shift[];


    constructor(

    ) {
    }

    ngOnInit() {

    }

    public getWorkerIds(shifts) {
        shifts.map(shift => {
            shift.workers.map(worker => {
                    this.workerIds.push(worker);
            });
        });
        return this.workerIds;
    }

    public getWorkers(shifts) {

        if (shifts.filter(shift => {
            return shift.workers.length > 0;
        }).length) {
            return true;
        }

    }


}