import {Action, createReducer, on} from '@ngrx/store';
import * as shiftActions from './shift.action';
import {initialState, shiftAdapter, ShiftState} from './shift.state';

export function shiftReducer(state: ShiftState | undefined, action: Action) {
  return reducer(state, action);
}

const reducer = createReducer(
  initialState,
  on(shiftActions.ShiftsChanged, (state, {shifts}) => {
    state = shiftAdapter.removeAll(state);
    return shiftAdapter.addMany(shifts, state);
  }),
);
