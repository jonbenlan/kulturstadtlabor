import {Injectable} from '@angular/core';
import * as ShiftActions from './shift.action';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {flatMap} from 'rxjs/operators';
import {ShiftService} from '../service/shift.service';
import * as fromShift from './shift.state';
import {Store} from '@ngrx/store';
import * as coreActions from '../../core/state/core.action';
import * as fromCore from '../../core/state/core.state';

@Injectable()
export class ShiftEffects {

  constructor(
    private actions$: Actions,
    private shiftService: ShiftService,
    private shiftStore: Store<fromShift.ShiftState>,
    private state: Store<fromCore.State>,
  ) {
  }

  @Effect({dispatch: false})
  add$ = this.actions$.pipe(
    ofType(ShiftActions.AddShift),
    flatMap(data => {
      return this.shiftService.addShift(data.shift)
        .pipe(res => {
            this.state.dispatch(coreActions.AddMessage({message: `Schicht erstellt`}));
            return res;
          }
        );
    })
  );

  @Effect({dispatch: false})
  updateWorkersWithLimitCheck$ = this.actions$.pipe(
    ofType(ShiftActions.UpdateWorkersWithLimitCheck),
    flatMap(data => {
      return this.shiftService.getShiftWorkers(data.shift).then(res => {
        // check if allowed number of workers is larger than the current number
        // of workers OR whether it's the last worker unsubscribing from the shift.
        // The latter then allows updating although the current number of workers is equal to the limit
        if (Number(data.shift.noOfWorker) >= res.length || data.shift.workers.length === 0) {
          return this.shiftService.updateShift(data.shift);
        } else {
          alert('Leider ist die Schicht in der Zwischenzeit bereits vergeben! Die Seite wird neu geladen und aktualisiert.');
          window.location.reload();
        }
      });
    }),
  );

  @Effect({dispatch: false})
  update$ = this.actions$.pipe(
    ofType(ShiftActions.UpdateShift),
    flatMap(data => {
      return this.shiftService.updateShift(data.shift)
        .pipe(res => {
            this.state.dispatch(coreActions.AddMessage({message: `Schicht angepasst`}));
            return res;
          }
        );
    }),
  );

  @Effect({dispatch: false})
  remove$ = this.actions$.pipe(
    ofType(ShiftActions.RemoveShift),
    flatMap(data => {
      return this.shiftService.removeShift(data.id);
    }),
  );

  @Effect({dispatch: false})
  removeShiftsForRessort$ = this.actions$.pipe(
    ofType(ShiftActions.RemoveShiftsForRessort),
    flatMap(data => {
      return this.shiftService.deleteShiftsByRessortID(data.ressortId);
    }),
  );
}
