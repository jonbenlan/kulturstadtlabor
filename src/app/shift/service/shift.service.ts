import {Injectable} from '@angular/core';

import {from, Subscription} from 'rxjs';
import {map} from 'rxjs/operators';
import {AngularFirestore} from '@angular/fire/firestore';
import * as ShiftActions from '../state/shift.action';
import * as fromShift from '../state/shift.state';
import {Shift} from '../../models/shift';
import {Store} from '@ngrx/store';

@Injectable({providedIn: 'root'})
export class ShiftService {

  private subscription: Subscription[] = [];
  private collectionName = 'shift';

  constructor(private store: Store<fromShift.ShiftState>, protected db: AngularFirestore) {
    this.clearSubscription();

    this.addSubscription(db.collection<Shift>(this.collectionName)
      .snapshotChanges()
      .pipe(map(actions => {
        return actions.map(action => {
          return {id: action.payload.doc.id, ...action.payload.doc.data()} as Shift;
        });
      })).subscribe((items) => this.shiftsChanged(items)));
  }

  clearSubscription() {
    this.subscription.forEach(x => x.unsubscribe());
    this.subscription = [];
  }

  addSubscription(subscription: Subscription) {
    this.subscription.push(subscription);
  }

  shiftsChanged(shifts: Shift[]) {
    this.store.dispatch(ShiftActions.ShiftsChanged({shifts}));
  }


  getShift(id: string) {
    return this.db.doc(`${this.collectionName}/${id}`);
  }

  addShift(shift: Shift) {
    const id = this.db.createId();
    shift.id = id;
    return from(this.getDoc(id).set(shift));
  }

  updateShift(shift) {
    return from(this.getDoc(shift.id).set(shift));
  }

  getShiftWorkers(shift: Shift) {
    const workers = this.getDoc(shift.id).ref.get().then( res => {
      const resultData = res.data();
      return resultData.workers;
    });
    return workers;
  }

  removeShift(id: string) {
    return from(this.getDoc(id).delete());
  }

  get collection() {
    return this.db.collection<Shift>(this.collectionName);
  }

  getDoc(id: string) {
    return this.db.doc(`${this.collectionName}/${id}`);
  }

  deleteShiftsByRessortID(ressortId: string) {
    const deleteShiftsByRessortQuery = this.db.collection(this.collectionName).ref.where('ressortId', '==', ressortId);
    return deleteShiftsByRessortQuery.get().then(querySnapshot => {
      querySnapshot.forEach(doc => {
        doc.ref.delete();
      });
    });
  }
}
