import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {Shift} from '../../../models/shift';
import * as fromShift from '../../../shift/state/shift.state';
import * as fromRessort from '../../../ressort/state/ressort.state';
import * as fromCore from '../../../core/state/core.state';
import {AuthService} from '../../../authentication/service/auth.service';
import {Store} from '@ngrx/store';
import {Ressort} from '../../../models/ressorts';
import {User} from 'firebase/app';

@Component({
  selector: 'app-my-shifts',
  templateUrl: './my-shifts.component.html'
})
export class MyShiftsComponent implements OnInit {

  ressorts$: Observable<Ressort[]>;
  user$: Observable<User>;

  constructor(
    public authService: AuthService,
    private shiftStore: Store<fromShift.ShiftState>,
    private ressortStore: Store<fromRessort.RessortState>,
    private coreStore: Store<fromCore.State>,
  ) {
  }

  ngOnInit() {
    this.ressorts$ = this.ressortStore.select(fromRessort.selectAll);
    this.user$ = this.coreStore.select(fromCore.selectUser);
  }

  getShiftsForUser(userId: string, confirmed: boolean): Observable<Shift[]> {
    return this.shiftStore.select((fromShift.selectShiftsForUser(userId, confirmed)));
  }
}
