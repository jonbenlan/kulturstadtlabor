import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import * as fromCore from '../../../core/state/core.state';
import {Observable} from 'rxjs';
import {Store} from '@ngrx/store';
import {User} from '../../../models/user';
import * as CoreActions from '../../../core/state/core.action';
import {AbstractControl, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {userProfileRoutesNames} from '../../user-profile.route.names';
import {additionalFieldInitData} from '../../../shared/additionalFields';

@Component({
  selector: 'app-edit-user-profile-page',
  templateUrl: './edit-user-profile.component.html',
  styleUrls: ['./edit-user-profile.component.scss']
})
export class EditUserProfilePageComponent implements OnInit {

  public _user;
  public form: FormGroup;

  private maxChars =  200;

  @Input()
  public set user(value) {
    this._user = value;
    this.ngOnInit();
  }

  public get user() {
    return this._user;
  }

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private store: Store<fromCore.State>,
    private fb: FormBuilder,
  ) {
  }

  ngOnInit() {

    const formGroup = {
      firstName: new FormControl(this.user.firstName, [Validators.required, Validators.maxLength(this.maxChars)]),
      lastName: new FormControl(this.user.lastName, [Validators.required, Validators.maxLength(this.maxChars)]),
      address: new FormControl(this.user.address, Validators.maxLength(this.maxChars)),
      place: new FormControl(this.user.place, Validators.maxLength(this.maxChars)),
      birthDate: new FormControl(this.user.birthDate, [Validators.required,
        Validators.pattern('^\\s*(3[01]|[12][0-9]|0?[1-9])\\.(1[012]|0?[1-9])\\.((?:19|20)\\d{2})\\s*$')]),
      phone: new FormControl(this.user.phone, [Validators.required, Validators.pattern('\\b\\d(?: ?\\d){9,12}\\b')])
    };

    additionalFieldInitData.forEach( field => {
      formGroup[field.id] = new FormControl(this.user[field.id]);
    });

    this.form = new FormGroup(formGroup);

  }

  public get firstNameControl(): AbstractControl {
    return this.form.get('firstName');
  }

  public get lastNameControl(): AbstractControl {
    return this.form.get('lastName');
  }

  public get birthDateControl(): AbstractControl {
    return this.form.get('birthDate');
  }

  public get phoneControl(): AbstractControl {
    return this.form.get('phone');
  }

  public get addressControl(): AbstractControl {
    return this.form.get('address');
  }

  public get placeControl(): AbstractControl {
    return this.form.get('place');
  }

  public get publicTransportControl(): AbstractControl {
    return this.form.get('materialTransport');
  }

  public get drivingLicenceControl(): AbstractControl {
    return this.form.get('drivingLicence');
  }

  public get peopleTransportControl(): AbstractControl {
    return this.form.get('peopleTransport');
  }

  public get hasCarControl(): AbstractControl {
    return this.form.get('hasCar');
  }

  public get flyerAreaControl(): AbstractControl {
    return this.form.get('flyerArea');
  }

  public get errorMessageFirstName() {
    let errorMessage: string;
    if (this.firstNameControl.hasError('required')) {
      errorMessage = 'Bitte gib deinen  Vornamen ein.';
    } else if (this.firstNameControl.hasError('maxlength')) {
      errorMessage = this.getErrorMessageTooManyChars();
    }
    return errorMessage;
  }

  public get errorMessageLastName() {
    let errorMessage: string;
    if (this.lastNameControl.hasError('required')) {
      errorMessage = 'Bitte gib deinen  Nachnamen ein.';
    } else if (this.lastNameControl.hasError('maxlength')) {
      errorMessage = this.getErrorMessageTooManyChars();
    }
    return errorMessage;
  }

  public get errorMessageBirthDate() {
    let errorMessage: string;
    if (this.birthDateControl.hasError('required')) {
      errorMessage = 'Bitte gib dein Geburtsdatum ein.';
    } else if (this.birthDateControl.hasError('pattern')) {
      errorMessage = 'Bitte gib ein gültiges Geburtsdatum ein.';
    }
    return errorMessage;
  }

  public get errorMessagePhone() {
    let errorMessage: string;
    if (this.phoneControl.hasError('required')) {
      errorMessage = 'Bitte eine Telefonnummer angeben.';
    } else if (this.phoneControl.hasError('pattern')) {
      errorMessage = 'Bitte gib eine gültige Nummer ein.';
    }
    return errorMessage;
  }

  public getErrorMessageTooManyChars(): string {
    return `Zu viele Zeichen (Maximal  ${this.maxChars} Zeichen).`;
  }


  isInvalid(value) {
    return this.form.controls[value].invalid
      && (this.form.controls[value].dirty || this.form.controls[value].touched);
  }

  async updateUser() {

    const user: User = {
      ...this.user,
      firstName: this.form.value.firstName,
      lastName: this.form.value.lastName,
      address: this.form.value.address,
      place: this.form.value.place,
      birthDate: this.form.value.birthDate,
      phone: this.form.value.phone,
      publicTransport: this.form.value.publicTransport,
      materialTransport: this.form.value.materialTransport,
      peopleTransport: this.form.value.peopleTransport,
      hasCar: this.form.value.hasCar,
      flyerArea: this.form.value.flyerArea
    };
    return this.store.dispatch(CoreActions.UpdateMetadataForUser({user}));
  }


  async onSubmit(id: string) {
    this.form.disable();
    this.updateUser();
    this.form.enable();
    this.navigateToDetailView(id);
  }

  private navigateToDetailView(id: string) {
    const path = userProfileRoutesNames.BASE_FULL.replace(':uid', id);
    this.router.navigate([path]);
  }
}

@Component({
  selector: 'app-edit-user-profile',
  template: `
      <app-edit-user-profile-page *ngIf="user$ | async" [user]="user$ | async"></app-edit-user-profile-page>
  `,
})

export class EditUserProfileComponent implements OnInit {
  public user$: Observable<User>;

  constructor(private store: Store<fromCore.State>, private route: ActivatedRoute) {
    const id = this.route.snapshot.paramMap.get('uid');
    if (id) {
      this.user$ = this.store.select(
        fromCore.selectMetadata);
    }
  }

  ngOnInit() {
  }
}

