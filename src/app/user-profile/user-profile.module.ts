import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {UserProfileRoutingModule} from './user-profile-routing.module';
import {UserProfileComponent} from './components/display-user-profile/user-profile.component';
import {
  EditUserProfileComponent,
  EditUserProfilePageComponent
} from './components/edit-user-profile/edit-user-profile.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {ReactiveFormsModule} from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatButtonModule, MatIconModule, MatInputModule, MatSelectModule} from '@angular/material';
import {MyShiftsComponent} from './components/my-shifts/my-shifts.component';
import {ShiftModule} from '../shift/shift.module';
import {BackButtonModule} from '../shared/back-button/back-button.module';
import {
  EditAdditionalFieldsComponent,
  EditAdditionalFieldsWrapperComponent
} from './components/edit-additional-fields/edit-additional-fields.component';
import {AdditionalFieldsComponent} from './components/additional-fields/additional-fields.component';


@NgModule({
  declarations: [UserProfileComponent, EditUserProfileComponent, EditUserProfilePageComponent, MyShiftsComponent, EditAdditionalFieldsComponent, EditAdditionalFieldsWrapperComponent, AdditionalFieldsComponent],
    imports: [
        CommonModule,
        UserProfileRoutingModule,
        FlexLayoutModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatIconModule,
        MatInputModule,
        MatFormFieldModule,
        ShiftModule,
        BackButtonModule,
        MatSelectModule
    ]
})
export class UserProfileModule { }
