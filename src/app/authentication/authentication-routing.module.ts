import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SignInComponent} from './components/sign-in/sign-in.component';
import {SignUpComponent} from './components/sign-up/sign-up.component';
import {ForgotPasswordComponent} from './components/forgot-password/forgot-password.component';
import {VerifyEmailComponent} from './components/verify-email/verify-email.component';
import {AuthPageComponent} from './components/auth-page/auth-page.component';
import {authRoutesNames} from './auth.route.names';
import {SecureInnerPagesGuard} from './guard/secure-inner-pages.guard';
import {AccessDeniedComponent} from './components/access-denied/access-denied.component';


const routes: Routes = [
  {
    path: '', component: AuthPageComponent, canActivate: [], children: [
      {path: authRoutesNames.SUB_SIGN_IN, component: SignInComponent, canActivate: [SecureInnerPagesGuard]},
      {path: authRoutesNames.SUB_SIGN_UP, component: SignUpComponent, canActivate: [SecureInnerPagesGuard]},
      {path: authRoutesNames.SUB_RESET_PASSWORD, component: ForgotPasswordComponent},
      {path: authRoutesNames.SUB_VERIFY_EMAIL, component: VerifyEmailComponent},
    ]
  },
  { path: authRoutesNames.SUB_ACCESS_DENIED, component: AccessDeniedComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthenticationRoutingModule {
}
