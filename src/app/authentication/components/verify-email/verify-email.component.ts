import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../service/auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {authRoutesNames} from '../../auth.route.names';
import {User} from 'src/app/models/user';
import {Observable} from 'rxjs';
import * as fromCore from '../../../core/state/core.state';
import {Store} from '@ngrx/store';


@Component({
  selector: 'app-verify-email',
  templateUrl: './verify-email.component.html'
})
export class VerifyEmailComponent implements OnInit {

  public user$: Observable<User>;

  constructor(
    public router: Router,
    public authService: AuthService,
    private route: ActivatedRoute,
    private store: Store<fromCore.State>
  ) { }

  ngOnInit() {
    this.user$ = this.store.select(
      fromCore.selectMetadata);
  }

  async sendVerificationMail() {
    this.authService.sendVerificationMail().then(() => {
        window.alert('Email successfully sent!');
    });
  }

  navigateToRegister() {
   this.router.navigate([authRoutesNames.SIGN_UP]);
  }
}
