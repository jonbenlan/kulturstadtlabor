import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormControl, FormGroup, Validators} from '@angular/forms';
import {authRoutesNames} from '../../auth.route.names';
import * as CoreActions from '../../../core/state/core.action';
import {Store} from '@ngrx/store';
import * as fromCore from '../../../core/state/core.state';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html'
})
export class ForgotPasswordComponent implements OnInit {

  constructor(
    public store: Store<fromCore.State>
  ) { }

  public form: FormGroup;
  public authRoutesNames = authRoutesNames;
  public formSent = false;


  ngOnInit() {

    this.form = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
    });
  }

  public get emailControl(): AbstractControl {
    return this.form.get('email');
  }

  public get errorMessageEmail() {

    let errorMessage: string;

    if (this.emailControl.hasError('required')) {
      errorMessage = 'Bitte eine Email angeben';
    } else if (this.emailControl.hasError('email')) {
      errorMessage = 'Bitte eine gültige Email angeben';
    }

    return errorMessage;

  }

  isInvalid(value) {
    return this.form.controls[value].invalid
      && (this.form.controls[value].dirty || this.form.controls[value].touched);
  }

  public resetPassword() {

    this.formSent = true;
    const email = this.form.value.email;
    return this.store.dispatch(CoreActions.ResetPassword({email}));

  }

}
