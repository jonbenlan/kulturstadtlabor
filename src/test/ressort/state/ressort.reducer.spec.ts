import * as fromRessort from '../../../app/ressort/state/ressort.state';
import * as fromRessortReducer from '../../../app/ressort/state/ressort.reducer';
import * as fromRessortActions from '../../../app/ressort/state/ressort.action';
import {Action} from '@ngrx/store';
import {Ressort} from '../../../app/models/ressorts';

describe('RessortReducer', () => {

  it('should not change initial state with no action', () => {
    const {initialState} = fromRessort;
    const action = {};
    const state = fromRessortReducer.ressortReducer(undefined, action as Action);
    expect(state).toBe(initialState);
  });


  it('should add a ressort to store', () => {
    const ressort: Ressort = {id: '1', title: 'testTitle', description: 'testDescripion'};
    const ressorts: Ressort[] = [ressort];
    const entities = {
      1: ressort,
    };
    const {initialState} = fromRessort;
    const action: Action = fromRessortActions.RessortsChanged({ressorts});
    const state = fromRessortReducer.ressortReducer(initialState, action);
    expect(state.entities).toEqual(entities);
  });

  it('should add multiple ressorts to store', () => {
    const ressortOne: Ressort = {id: '1', title: 'testTitleOne', description: 'testDescripionOne'};
    const ressortTwo: Ressort = {id: '2', title: 'testTitleTwo', description: 'testDescripionTwo'};
    const ressorts: Ressort[] = [ressortOne, ressortTwo];
    const entities = {
      1: ressortOne,
      2: ressortTwo,
    };
    const {initialState} = fromRessort;
    const action: Action = fromRessortActions.RessortsChanged({ressorts});
    const state = fromRessortReducer.ressortReducer(initialState, action);
    expect(state.entities).toEqual(entities);
  });
});
