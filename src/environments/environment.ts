// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: true,
    firebaseConfig: {
		apiKey: "AIzaSyAOdb-8Ew03829x4hzi2tyvgoE2OreDipE",
		authDomain: "mithelfen-kulturstadtlabor.firebaseapp.com",
		databaseURL: "https://mithelfen-kulturstadtlabor.firebaseio.com",
		projectId: "mithelfen-kulturstadtlabor",
		storageBucket: "mithelfen-kulturstadtlabor.appspot.com",
		messagingSenderId: "342416654874",
		appId: "1:342416654874:web:8f3f1a29404cb9b82d07a3"
  }
};




/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
